import pygame, random, os, keyboard, string
#Sound effects
pygame.mixer.init()
#Window size, FPS
WIDTH, HEIGHT = 1280, 720
FPS = 60
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Bubble Typer")
BACKGROUND_IMAGE = pygame.image.load(os.path.join("assets", "background.jpg"))
WIN.blit(BACKGROUND_IMAGE,(0,0))
pygame.font.init()
myfont3 = pygame.font.SysFont("consolas", 76)
clock = pygame.time.Clock()
#Sound effects
pygame.mixer.init()
#Window size, FPS
WIDTH, HEIGHT = 1280, 720
FPS = 60
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Bubble Typer")
BACKGROUND_IMAGE = pygame.image.load(os.path.join("assets", "background.jpg"))
WIN.blit(BACKGROUND_IMAGE,(0,0))
pygame.font.init()
myfont3 = pygame.font.SysFont("consolas", 76)
clock = pygame.time.Clock()
#Song and sound effects
background_song = pygame.mixer.Sound(os.path.join("assets", "background_song.mp3"))
background_song.play(-1)
select_sound = pygame.mixer.Sound(os.path.join("assets", "select.wav"))
lose_sound = pygame.mixer.Sound(os.path.join("assets", "lose.wav"))

#Title font and total pop font
myfont = pygame.font.SysFont("consolas", 16)
myfont2 = pygame.font.SysFont("consolas", 76)

def main():
    
    #Generate first letter and 
    key_press = random.choice(string.ascii_letters)
    
    #Bubble variables
    BUBBLE_IMAGE = pygame.image.load(os.path.join("assets", "bubble.png"))
    BUBBLE_POP_IMAGE = pygame.image.load(os.path.join("assets", "popped_bubble.png"))
    BUBBLE_POP = pygame.transform.scale(BUBBLE_POP_IMAGE, (200,200))
    BUBBLE = pygame.transform.scale(BUBBLE_IMAGE, (200,200))
    bubble_speed = 2
    bubble_pop_sound = pygame.mixer.Sound(os.path.join("assets", "pop.wav"))
    bubble = pygame.Rect(random.randint(0,1080),720,200,200)
    pop_frames = 10

    disable_left = False
    disable_right = True
    bubble_right = 0
    bubble_left = 0
#Score
    total_pops = 0
    
    
         
    while True:
        clock.tick(FPS)
#If the python shell window is closed or escapse is pressed,
#close the game window
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
            

                  
            
 
#---------Bubble logic-------------
#pop_frames are for making sure the pop animation stays on screen for long enough.
#It is absolutely not a good idea to do it like this, but its all i know how to do right now    
        pop_frames += 1
#Makes bubble move up        
        bubble.y -= bubble_speed
#Makes bubble wave side to side
                
        if disable_left == False:
            bubble.x -= 1
            bubble_left += 1
        elif disable_right == False:
            bubble.x += 1
            bubble_right += 1
            
        if bubble_left > 50 and disable_right == True:
            disable_left = True
            disable_right = False
            bubble_right = 0
            
        
        elif bubble_right > 50 and disable_left == True:
            disable_left = False
            disable_right = True
            bubble_left = 0
#This detects when the key pressed matches the key in key_press for the bubble
        if keyboard.is_pressed(key_press): 
            bubble_pop_sound.play()
            total_pops += 1
#bubble_pop_coords is just so the animation summons at the position of the bubble
            bubble_pop_coords = (bubble.x, bubble.y)
            bubble = pygame.Rect(random.randint(50,1030),720,200,200)
            pop_frames = 0
            bubble_speed += 0.2
            #Add a little delay after every pop, this helps the player process things
            #It also gives them a small break, to prepare for next bubble
        
            pygame.time.delay(150)
#Pop animation, not too happy with this but its ok for now, wish it stayed around a little longer
        if pop_frames <= 5:
            WIN.blit(BUBBLE_POP,(bubble_pop_coords))
            
#Randomly picks new letter
            key_press = random.choice(string.ascii_letters)

            
#Lose detection, lose funtion is also used when restarting the game     

        if (bubble.y + 220) <= 0:
            lose_sound.play()
            global lose
            lose = True
            break
           
    

#----------- Draw Window ---------------            

#The order in which things are drawn is important, its like layers
            
        
        
        pygame.display.update()
        #Make window green
        WIN.blit(BACKGROUND_IMAGE,(0,0))
        #Total pop text. Create then display
        text = myfont.render("Total Pops: " + str(total_pops), 1, (0,0,0))
        WIN.blit(text, (5, 10))
        #Move the bubble asset to the bubble object
        WIN.blit(BUBBLE, (bubble.x, bubble.y))
        #Letters on the bubble. Create then display
        bubble_text = myfont2.render(key_press.upper(), 1, (0,0,0))
        WIN.blit(bubble_text,((bubble.x + 77),(bubble.y+62)))
        
#-------------Press enter to start code-----------       
start = False
start_text1 = myfont2.render("Bubble Typer", 1, (0,0,0))
start_text2 = myfont.render("Press the correct key on the keyboard", 1, (0,0,0))
start_text3 = myfont.render("to pop the bubbles and save the castle", 1, (0,0,0))
start_text4 = myfont.render("Press Enter to start", 1, (0,0,0))
start_text5 = myfont.render("Press Escape to Quit", 1, (0,0,0))
    
WIN.blit(start_text1,(200,200))
WIN.blit(start_text2,(200,300))
WIN.blit(start_text3,(200,320))
WIN.blit(start_text4,(200,360))
WIN.blit(start_text5,(200,380))
pygame.display.update()
while start == False:
    clock.tick(FPS / 6)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                select_sound.play()
                start = True
                break
            elif event.key == pygame.K_ESCAPE:
                select_sound.play()
                pygame.quit()
 
main()
#-------------Restart game or quit code-----------  
while lose == True:
    clock.tick(FPS / 6)
    restart_text = myfont2.render("Restart? Y/N", 1, (0,0,0))
    WIN.blit(restart_text, (350,300))
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_y:
                select_sound.play()
                lose = False
                main()
        #elif event.type == pygame.KEYDOWN:
            elif event.key == pygame.K_n:
                select_sound.play()
                pygame.quit()

    pygame.display.update()




#Song and sound effects
background_song = pygame.mixer.Sound(os.path.join("assets", "background_song.mp3"))
background_song.play(-1)
select_sound = pygame.mixer.Sound(os.path.join("assets", "select.wav"))
lose_sound = pygame.mixer.Sound(os.path.join("assets", "lose.wav"))

#Title font and total pop font
myfont = pygame.font.SysFont("consolas", 16)
myfont2 = pygame.font.SysFont("consolas", 76)

def main():
    
    #Generate first letter and another random letter for preventing 2 of the same letter in a row
    key_press = random.choice(string.ascii_letters)
    previous_key = random.choice(string.ascii_letters)
    #Bubble variables
    BUBBLE_IMAGE = pygame.image.load(os.path.join("assets", "bubble.png"))
    BUBBLE_POP_IMAGE = pygame.image.load(os.path.join("assets", "popped_bubble.png"))
    BUBBLE_POP = pygame.transform.scale(BUBBLE_POP_IMAGE, (200,200))
    BUBBLE = pygame.transform.scale(BUBBLE_IMAGE, (200,200))
    bubble_speed = 2
    bubble_pop_sound = pygame.mixer.Sound(os.path.join("assets", "pop.wav"))
    bubble = pygame.Rect(random.randint(0,1080),720,200,200)
    pop_frames = 10

    disable_left = False
    disable_right = True
    bubble_right = 0
    bubble_left = 0
#Score
    total_pops = 0
    
    
         
    while True:
        clock.tick(FPS)
#If the python shell window is closed or escapse is pressed,
#close the game window
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
            

                  
            
 
#---------Bubble logic-------------
#pop_frames are for making sure the pop animation stays on screen for long enough.
#It is absolutely not a good idea to do it like this, but its all i know how to do right now    
        pop_frames += 1
#Makes bubble move up        
        bubble.y -= bubble_speed
#Makes bubble wave side to side
                
        if disable_left == False:
            bubble.x -= 1
            bubble_left += 1
        elif disable_right == False:
            bubble.x += 1
            bubble_right += 1
            
        if bubble_left > 50 and disable_right == True:
            disable_left = True
            disable_right = False
            bubble_right = 0
            
        
        elif bubble_right > 50 and disable_left == True:
            disable_left = False
            disable_right = True
            bubble_left = 0
#This detects when the key pressed matches the key in key_press for the bubble
        if keyboard.is_pressed(key_press): 
            bubble_pop_sound.play()
            total_pops += 1
#bubble_pop_coords is just so the animation summons at the position of the bubble
            bubble_pop_coords = (bubble.x, bubble.y)
            bubble = pygame.Rect(random.randint(50,1030),720,200,200)
            pop_frames = 0
            bubble_speed += 0.2
            #Add a little delay after every pop, this helps the player process things
            #It also gives them a small break, to prepare for next bubble
        
            pygame.time.delay(100)
#Pop animation, not too happy with this but its ok for now, wish it stayed around a little longer
        if pop_frames <= 5:
            WIN.blit(BUBBLE_POP,(bubble_pop_coords))
            
#Randomly picks new letter
            key_press = random.choice(string.ascii_letters)

            
#Lose detection, lose funtion is also used when restarting the game     

        if (bubble.y + 220) <= 0:
            lose_sound.play()
            global lose
            lose = True
            break
           
    

#----------- Draw Window ---------------            

#The order in which things are drawn is important, its like layers
            
        
        
        pygame.display.update()
        #Display background image
        WIN.blit(BACKGROUND_IMAGE,(0,0))
        #Total pop text. Create then display
        text = myfont.render("Total Pops: " + str(total_pops), 1, (0,0,0))
        WIN.blit(text, (5, 10))
        #Move the bubble asset to the bubble object
        WIN.blit(BUBBLE, (bubble.x, bubble.y))
        #Letters on the bubble. Create then display
        bubble_text = myfont2.render(key_press.upper(), 1, (0,0,0))
        WIN.blit(bubble_text,((bubble.x + 77),(bubble.y+62)))
        
#-------------Press enter to start code-----------       
start = False
start_text1 = myfont2.render("Bubble Typer", 1, (0,0,0))
start_text2 = myfont.render("Press the correct key on the keyboard", 1, (0,0,0))
start_text3 = myfont.render("to pop the bubbles and save the castle", 1, (0,0,0))
start_text4 = myfont.render("Press Enter to start", 1, (0,0,0))
start_text5 = myfont.render("Press Escape to Quit", 1, (0,0,0))
    
WIN.blit(start_text1,(200,200))
WIN.blit(start_text2,(200,300))
WIN.blit(start_text3,(200,320))
WIN.blit(start_text4,(200,360))
WIN.blit(start_text5,(200,380))
pygame.display.update()
while start == False:
    clock.tick(FPS / 6)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                select_sound.play()
                start = True
                break
            elif event.key == pygame.K_ESCAPE:
                select_sound.play()
                pygame.quit()
 
main()
#-------------Restart game or quit code-----------  
while lose == True:
    clock.tick(FPS / 6)
    restart_text = myfont2.render("Restart? Y/N", 1, (0,0,0))
    WIN.blit(restart_text, (350,300))
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_y:
                select_sound.play()
                lose = False
                main()
        #elif event.type == pygame.KEYDOWN:
            elif event.key == pygame.K_n:
                select_sound.play()
                pygame.quit()

    pygame.display.update()

